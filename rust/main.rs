#![deny(missing_docs)]
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! u8loc - run a command in a UTF-8-capable locale

use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::os::unix::process::CommandExt;
use std::process::{self, Command};

#[macro_use]
extern crate quick_error;

use getopts::Options;
use utf8_locale::{LanguagesDetect, Utf8Detect};

quick_error! {
    #[derive(Debug)]
    enum UErr {
        ExactlyOne {
            display("Exactly one of the -q and -r options must be specified")
        }
        InvalidQuery(name: String) {
            display("Invalid query name '{}' specified", name)
        }
        NoProgram {
            display("No program specified to run")
        }
    }
}

#[derive(Debug)]
enum Mode {
    Features,
    QueryEnv(String, bool),
    QueryList,
    QueryPreferred,
    Run(Vec<String>, bool),
}

#[derive(Debug)]
struct Config {
    mode: Mode,
}

fn parse_args() -> Result<Config, Box<dyn Error>> {
    let mut parser = Options::new();

    parser.optflag(
        "",
        "features",
        "display the features supported by the program and exit",
    );
    parser.optflag(
        "p",
        "",
        "use a locale specified in the LANG and LC_* variables if appropriate",
    );
    parser.optopt(
        "q",
        "",
        "output the value of an environment variable",
        "NAME",
    );
    parser.optflag(
        "r",
        "",
        "run the specified program in a UTF-8-friendly environment",
    );

    let args: Vec<String> = env::args().collect();
    let opts = parser.parse(&args[1..])?;

    let preferred = opts.opt_present("p");
    match opts.opt_get::<String>("q")? {
        Some(value) => match opts.opt_present("r") {
            true => Err(Box::new(UErr::ExactlyOne)),
            false => match &*value {
                "list" => Ok(Config {
                    mode: Mode::QueryList,
                }),
                "preferred" => Ok(Config {
                    mode: Mode::QueryPreferred,
                }),
                value @ ("LC_ALL" | "LANGUAGE") => Ok(Config {
                    mode: Mode::QueryEnv(value.to_string(), preferred),
                }),
                other => Err(Box::new(UErr::InvalidQuery(other.to_string()))),
            },
        },
        None => match opts.opt_present("r") {
            true => match opts.free.is_empty() {
                true => Err(Box::new(UErr::NoProgram)),
                false => Ok(Config {
                    mode: Mode::Run(opts.free, preferred),
                }),
            },
            false => match opts.opt_present("features") {
                true => Ok(Config {
                    mode: Mode::Features,
                }),
                false => Err(Box::new(UErr::ExactlyOne)),
            },
        },
    }
}

fn show_features() -> Result<(), Box<dyn Error>> {
    println!(
        "Features: u8loc={} query-env=0.1 query-preferred=0.1 run=0.1",
        env!("CARGO_PKG_VERSION")
    );
    Ok(())
}

fn get_env(preferred: bool) -> Result<HashMap<String, String>, Box<dyn Error>> {
    let det = match preferred {
        true => {
            let langs = LanguagesDetect::new().detect()?;
            Utf8Detect::new().with_languages(langs)
        }
        false => Utf8Detect::new(),
    };
    Ok(det.detect()?.env)
}

fn query_env(name: &str, preferred: bool) -> Result<(), Box<dyn Error>> {
    let env = get_env(preferred)?;
    match env.get(name) {
        None => panic!("Internal error: {:?} should be present in {:?}", name, env),
        Some(value) => println!("{}", value),
    };
    Ok(())
}

fn query_list() -> Result<(), Box<dyn Error>> {
    println!("LANGUAGE  - The LANGUAGE environment variable");
    println!("LC_ALL    - The LC_ALL environment variable");
    println!("list      - List the available query parameters");
    println!("preferred - List the preferred languages as per the locale variables");
    Ok(())
}

fn query_preferred() -> Result<(), Box<dyn Error>> {
    let langs = LanguagesDetect::new().detect()?;
    println!("{}", langs.join("\n"));
    Ok(())
}

fn run_program(prog: &[String], preferred: bool) -> Result<(), Box<dyn Error>> {
    let env = get_env(preferred)?;
    Err(Box::new(
        Command::new(&prog[0])
            .args(&prog[1..])
            .env_clear()
            .envs(env)
            .exec(),
    ))
}

fn run() -> Result<(), Box<dyn Error>> {
    let cfg = parse_args()?;
    match &cfg.mode {
        Mode::Features => show_features(),
        Mode::QueryEnv(name, preferred) => query_env(name, *preferred),
        Mode::QueryList => query_list(),
        Mode::QueryPreferred => query_preferred(),
        Mode::Run(prog, preferred) => run_program(prog, *preferred),
    }
}

fn main() {
    if let Err(err) = run() {
        eprintln!("{}", err);
        process::exit(1);
    }
}
