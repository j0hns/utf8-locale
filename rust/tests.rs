/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::collections::HashMap;
use std::error::Error;

use lazy_static::lazy_static;

use super::LanguagesDetect;

#[derive(Debug)]
struct TDataLang {
    env: HashMap<String, String>,
    expected: Vec<String>,
}

#[derive(Debug)]
struct TData {
    languages: Vec<TDataLang>,
}

fn load_test_data() -> Result<TData, Box<dyn Error>> {
    Ok(TData { languages: vec![] })
}

fn get_test_data() -> &'static TData {
    lazy_static! {
        static ref TEST_DATA: TData = load_test_data().unwrap();
    }
    &*TEST_DATA
}

#[test]
fn test_preferred() -> Result<(), Box<dyn Error>> {
    for tcase in &get_test_data().languages {
        assert_eq!(
            LanguagesDetect::new()
                .with_env(tcase.env.clone())
                .detect()?,
            tcase.expected
        );
    }
    Ok(())
}
