# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set(CPPFLAGS_STD -D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700 CACHE STRING "C preprocessor flags defining standards compliance")
set(CFLAGS_STD -std=c99 CACHE STRING "C compiler flags specifying the C dialect to use")

set(CFLAGS_WARN_GCC -Wall -W)
set(CFLAGS_WARN_BDE -Wall -W -pedantic -Wbad-function-cast -Wcast-align -Wcast-qual -Wchar-subscripts -Winline -Wmissing-prototypes -Wnested-externs -Wpointer-arith -Wredundant-decls -Wshadow -Wstrict-prototypes -Wwrite-strings)

option(USE_BDE_CFLAGS "Add some use GCC compiler warning flags for stricter checks" OFF)
if(USE_BDE_CFLAGS)
	set(CFLAGS_WARN_DEFAULT ${CFLAGS_WARN_BDE})
else()
	set(CFLAGS_WARN_DEFAULT ${CFLAGS_WARN_GCC})
endif()

option(USE_WERROR "Stop the build if any compiler warnings are emitted" OFF)
if(USE_WERROR)
	set(CFLAGS_WARN_WERROR -Werror)
else()
	set(CFLAGS_WARN_WERROR)
endif()

set(CFLAGS_WARN ${CFLAGS_WARN_DEFAULT} ${CFLAGS_WARN_WERROR} CACHE STRING "C compiler warning flags")

set(CPPFLAGS CACHE STRING "Base C preprocessor flags")
set(CPPFLAGS_FULL $CACHE{CPPFLAGS} $CACHE{CPPFLAGS_STD})

set(CFLAGS -g -pipe CACHE STRING "Base C compiler flags")
set(CFLAGS_FULL $CACHE{CFLAGS} $CACHE{CFLAGS_STD} $CACHE{CFLAGS_WARN})

set(U8_INCLUDEDIR "${CMAKE_CURRENT_SOURCE_DIR}/include")

add_subdirectory(u8loc)
add_subdirectory(utf8_locale)
